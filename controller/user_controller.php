<?php include '../view/header_user.php';

session_start();

$action = filter_input(INPUT_POST, 'action');
if($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL){
        $action = 'home';
    }
}


if($action == 'account') {
    require '../view/account.php';
} else if($action == 'home') {
    require '../view/home.php';
} else if($action == 'trophy') {
    require '../view/trophy.php';
} else if($action == 'news') {
    require '../view/news.php';
} else if($action == 'error') {
    require '../view/error.php';
} else if($action == 'logout') {
    require '../view/logOut.php';
    session_destroy();
}


include '../view/footer.php';

