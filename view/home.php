  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
                <h1 class="header center blue-grey-text">Playstation Trophies<h1>
      <div class="row center">
            <h5 class="header col s12 light">Track Your Progress!</h5>
      </div>
      <div class="row center">
          <br>
        <a href="news.php"><img src="../images/main_news.jpg" alt="Main News Story" width ="55%"/></a>
        <h6 class="header col s12"> Main News Article </h6>
      </div>

    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row right-align">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">forum</i></h2>
            <h5 class="center">Forums</h5>

            <p class="light">Extra cheeky banter with the lads</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">timeline</i></h2>
            <h5 class="center">Twitter Feed</h5>

            <a class="twitter-timeline" href="https://twitter.com/BantPlaystation" data-widget-id="721327569711271938">Tweets by @BantPlaystation</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

          </div>
        </div>
          
        <div class="col s12 m4">
            <div class="icon-block">
              <h2 class="center light-blue-text"><i class="material-icons">games</i></h2>
              <h5 class="center">Games</h5>

            </div>
        </div>
      </div>

    </div>
    <br><br>

    <div class="section">

    </div>
  </div>