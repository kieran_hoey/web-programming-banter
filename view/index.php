<?php include 'header.php'; ?>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
                <h1 class="header center blue-grey-text">Playstation Trophies<h1>
      <div class="row center">
            <h5 class="header col s12 light">Track Your Progress!</h5>
      </div>
      <div class="row center">
        
      </div>
      <br><br>

    </div>
  </div>


  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Banter bant</h5>

            <p class="light">Extra cheeky banter with the lads</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">User Experience Banter</h5>

            <p class="light">Cheeky banter with Myself</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
            <h5 class="center">Banter work</h5>

            <p class="light">Banter banter banter banter banter</p>
          </div>
        </div>
      </div>

    </div>
    <br><br>

    <div class="section">

    </div>
  </div>
<?php include 'footer.php'; ?>