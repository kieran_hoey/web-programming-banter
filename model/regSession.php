<?php
require '../model/database.php';
session_start();
    
    $usr = filter_input(INPUT_POST, 'username');
    $pw = filter_input(INPUT_POST, 'password');
    $n = filter_input(INPUT_POST, 'user_name');
    $email = filter_input(INPUT_POST, 'user_email');
    $tel = filter_input(INPUT_POST, 'user_phoneNumber');
    
    
if($usr == NULL || $pw == NULL || $n == NULL || $email == NULL || $tel == NULL)
    {
        header("Location: ../controller/general_controller.php?action=error");
    }
else
    {
        if(insert_new_user($usr, $pw, $n, $email, $tel) == false)
        {
            header("Location: ../controller/general_controller.php?action=error");
        }
        else
        {
            header("Location: ../controller/general_controller.php?action=home");
        }

    }

function insert_new_user($usr, $pw, $n, $email, $tel)
{
    global $db;
    $query = "INSERT INTO users (username, password, user_name, user_email, user_phoneNumber) VALUES (:username, :password, :user_name, :user_email, :user_phoneNumber)";
    $statement = $db->prepare($query);
    $statement->bindValue(":username", $usr);
    $statement->bindValue(":password", $pw);
    $statement->bindValue(":user_name", $n);
    $statement->bindValue(":user_email", $email);
    $statement->bindValue(":user_phoneNumber", $tel);
    $statement->execute();
    $statement->closeCursor();
    
    return true;
}