<div class="section no-pad-bot" id="index-banner">
    <div class="container">
          <div class="row">
    <form action="../model/regSession.php" method="post" class="col s12">
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">account_circle</i>
          <input id="account_circle" type="text" name="username" class="validate" required>
          <label for="account_circle">Username</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">lock</i>
          <input id="lock" type="password" name="password" class="validate" required>
          <label for="lock">Password</label>
      </div>
      </div>
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">perm_identity</i>
          <input id="perm_identity" type="text" name="user_name" class="validate" required>
          <label for="perm_identity">Name</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">email</i>
          <input id="email" type="email" name="user_email" class="validate" required>
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">call</i>
          <input id="call" type="tel" name="user_phoneNumber" class="validate" required>
          <label for="call">Phone Number</label>
        </div>
      </div>
      <div class="row">
        <div class="col s6 push-s5">
            <input class="btn" type="submit" value="Register">
        </div>
      </div>
      </form>
  </div>
    </div>
</div>