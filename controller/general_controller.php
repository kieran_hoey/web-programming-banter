<?php include '../view/header.php';

require '../model/database.php';


$lifetime = 60 * 60;
$path = "/";
$domain = "";
$secure = FALSE;
session_set_cookie_params($lifetime, $path, $domain, $secure);
session_start();

$action = filter_input(INPUT_POST, 'action');
if($action == NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action == NULL){
        $action = 'home';
    }
}


if($action == 'login') {
    require '../view/logIn.php';
} else if($action == 'home') {
    require '../view/home.php';
} else if($action == 'trophy') {
    require '../view/trophy.php';
} else if($action == 'news') {
    require '../view/news.php';
} else if($action == 'error') {
    require '../view/error.php';
} else if($action == 'register') {
    require '../view/register.php';
}

include '../view/footer.php'; 
session_destroy();

