<div class="section no-pad-bot" id="index-banner">
    <div class="container">
          <div class="row">
    <form action="../model/checkSessions.php" method="post" class="col s12">
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">account_circle</i>
          <input id="account_circle" type="text" name="username" class="validate">
          <label for="account_circle">Username</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s8 push-s2">
          <i class="material-icons prefix">lock</i>
          <input id="lock" type="password" name="password" class="validate">
          <label for="lock">Password</label>
        </div>
      <div class="row">
        <div class="col s6 push-s5">
            <input class="btn" type="submit" value="Submit">
        </div>
      </div>
      </form>
      <form action="../controller/general_controller.php?action=register" method="post">
      <div class="row">
        <div class="col s6 push-s5">
            <input class="btn" type="submit" value="Register">
        </div>
      </div>
    </form>
    
  </div>
    </div>
</div>


