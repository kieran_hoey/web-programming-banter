<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
        <title>Website</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>
    <body>
        <nav class="blue-grey darken-2" role="navigation">
            <div class="nav-wrapper container"><a id="logo-container" href="../controller/user_controller.php" class="brand-logo"><img src="../images/playstation_logo_2.png" alt="Playstation Logo" width ="60"/></a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="?action=home">Home</a></li>
                    <li><a href="?action=trophy">Trophy</a></li>
                    <li><a href="?action=news">News</a></li>
                    <li>`<a href="?action=account" class="chip"><img src="<?php?>" alt="Contact Person">Banter</a></li>
                    <li><a href="?action=logout">Log Out</a></li>
                </ul>

                <ul id="nav-mobile" class="side-nav">
                    <li><a href="?action=home">Home</a></li>
                    <li><a href="?action=trophy">Trophy</a></li>
                    <li><a href="?action=news">News</a></li>
                    <li><a href="?action=account">My Account</a></li>
                    <li><a href="?action=logout">Log Out</a></li>
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        
        <main>

