<?php
require '../model/database.php';
session_start();

    $usr= filter_input(INPUT_POST, 'username');
    $pw = filter_input(INPUT_POST, 'password');
    
if($usr == NULL || $pw == NULL)
    {
        header("Location: ../controller/general_controller.php?action=error");
    }
else 
    {
        if(check_usr_pw($usr, $pw) == false)
        {
            header("Location: ../controller/general_controller.php?action=error");
        }
        else
        {
            header("Location: ../controller/user_controller.php");
        }
        
    }
if(isset($_SESSION['user']))
    {
        $usr = $_SESSION['user'];
    }

function check_usr_pw($usr, $pw)
{
    global $db;
    $query = "SELECT user_id FROM users WHERE username = :username and password = :password";
    $statement = $db->prepare($query);
    $statement->bindValue(":username", $usr);
    $statement->bindValue(":password", $pw);
    $statement->execute();
    $id = $statement->fetch();
    $statement->closeCursor();

    if($id == NULL)
    {
        return false;
    }
    
    return true;
}